package com.mani.mkmatrimony.register;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.mani.mkmatrimony.register.fragment.FragmentFour;
import com.mani.mkmatrimony.register.fragment.FragmentOne;
import com.mani.mkmatrimony.register.fragment.FragmentThree;
import com.mani.mkmatrimony.register.fragment.FragmentTwo;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class Signup_StepperAdapter extends AbstractFragmentStepAdapter {

    public Signup_StepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:

                return FragmentOne.newInstance();
            case 1:

                return FragmentTwo.newInstance();
            case 2:

                return FragmentThree.newInstance();
            case 3:

                return FragmentFour.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

   /* @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Basic") //can be a CharSequence instead
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Additional") //can be a CharSequence instead
                        .create();
            case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("Partner") //can be a CharSequence instead
                        .create();
        }
        return null;
    }*/
}