package com.mani.mkmatrimony.register.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.MediaRouteButton;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.credentials.IdentityProviders;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mani.mkmatrimony.R;
import com.mani.mkmatrimony.helper.CustomButton;
import com.mani.mkmatrimony.register.adapter.RegisterButtonAdapter;
import com.mani.mkmatrimony.register.adapter.RegisterButtonAdapter.itemClick;
import com.mani.mkmatrimony.register.interfaces.OnFragmentInteractionListener;
import com.mani.mkmatrimony.register.model.ModelOne;
import com.mani.mkmatrimony.register.pickview.LoopView;
import com.mani.mkmatrimony.register.pickview.popwindow.DatePickerPopWin;
import com.mani.mkmatrimony.util.LoadingIndicator;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.text.SimpleDateFormat;

import java.util.Calendar;

public class FragmentOne extends Fragment implements Step ,itemClick, View.OnClickListener, View.OnTouchListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener ,RegisterButtonAdapter.SmartSignInterface {


    private OnFragmentInteractionListener mListener;
    private GridView glProfileCreated;
    private Context context;
    private RegisterButtonAdapter registerAdapter;
    private CustomButton genderMale;
    private CustomButton genderFemale;
    private EditText reg_name_editText;
    private TextView reg_countrycode_editText;
    private EditText reg_mobile_editText;
    private EditText reg_email_editText;
    private EditText reg_password_editText;
    private EditText reg_dob_editText;
    private EditText reg_religion_editText;
    private EditText reg_denomination_editText;
    private EditText reg_caste_editText;
    private EditText reg_subcaste_editText;
    private EditText reg_mothertongue_editText;
    private EditText reg_optional_editText;
    private EditText reg_other_denomination_editText;
    private EditText reg_other_caste_editText;
    private TextView reg_community_button;
    private Button reg_next_button;
    private Button amrithdhari_button;
    private Button sehajadhar_button;
    private MediaRouteButton reg_login_button;
    private Button male_button;
    private Button female_button;
    private ImageView eye;
    private ImageView mobile_help;
    private ModelOne one;
    private Calendar calendar;
    private boolean cliked = false;
    private Calendar demmyCalender;
    private int year;
    private int month;
    private int day;
    private String DOBdate;
    private SimpleDateFormat dateFormat;
    private String currentDate;
    private static final String TAG = FragmentOne.class.getSimpleName();
    private GoogleApiClient mCredentialsApiClient;
    private static final int RC_HINT = 2;
    private boolean emailNone =false;
    private boolean mobileNone =false;

    @Nullable
    @Override
    public VerificationError verifyStep() {
        mListener.onFragmentInteraction(1,FragmentOne.this);



        return null;
    }

    @Override
    public void onSelected() {


    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity;
        one=new ModelOne() ;
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onError(@NonNull VerificationError error) {

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.registerone, container, false);

        initialView(view);
        iniitData();
        return view;
    }

    private void iniitData() {
        registerAdapter  = new RegisterButtonAdapter(this.context.getResources().getStringArray(R.array.Profile_Created_BY_Array), this.context,this);
        this.glProfileCreated.setAdapter(registerAdapter);
        registerAdapter.getValuse(this);
    }

    private void initialView(View view) {

        this.glProfileCreated = (GridView) view.findViewById(R.id.glProfileCreated);
        this.reg_name_editText = (EditText) view.findViewById(R.id.reg_name_editText);
        this.reg_countrycode_editText = (TextView) view.findViewById(R.id.reg_countrycode_editText);
        this.reg_mobile_editText = (EditText) view.findViewById(R.id.reg_mobile_editText);
        this.reg_email_editText = (EditText) view.findViewById(R.id.reg_email_editText);
        this.reg_password_editText = (EditText) view.findViewById(R.id.reg_password_editText);
        this.reg_dob_editText = (EditText) view.findViewById(R.id.reg_dob_editText);
        this.reg_religion_editText = (EditText) view.findViewById(R.id.reg_religion_editText);
        this.reg_denomination_editText = (EditText) view.findViewById(R.id.reg_denomination_editText);
        this.reg_caste_editText = (EditText) view.findViewById(R.id.reg_caste_editText);
        this.reg_subcaste_editText = (EditText) view.findViewById(R.id.reg_subcaste_editText);
        this.reg_mothertongue_editText = (EditText) view.findViewById(R.id.reg_mothertongue_editText);
        this.reg_optional_editText = (EditText) view.findViewById(R.id.reg_other_subcaste_editText);
        this.reg_optional_editText.setVisibility(View.GONE);
        this.reg_other_denomination_editText = (EditText) view.findViewById(R.id.reg_other_denomination_editText);
        this.reg_other_denomination_editText.setVisibility(View.GONE);
        this.reg_other_caste_editText = (EditText) view.findViewById(R.id.reg_other_caste_editText);
        this.reg_other_caste_editText.setVisibility(View.GONE);
        this.reg_community_button = (TextView) view.findViewById(R.id.reg_community_button);
        this.reg_next_button = (Button) view.findViewById(R.id.reg_next_button);
        this.amrithdhari_button = (Button) view.findViewById(R.id.amrithdhari_button);
        this.sehajadhar_button = (Button) view.findViewById(R.id.sehajadhar_button);
        //this.reg_login_button = (Button) view.findViewById(R.id.reg_login_button);
      //  this.reg_login_button.setVisibility(View.GONE);
        this.male_button = (Button) view.findViewById(R.id.male_button);
        this.female_button = (Button) view.findViewById(R.id.female_button);

        this.eye = (ImageView) view.findViewById(R.id.password_view_hide);
        this.mobile_help = (ImageView) view.findViewById(R.id.mobile_help);


        this.amrithdhari_button.setOnClickListener(this);
        this.sehajadhar_button.setOnClickListener(this);
        this.reg_dob_editText.setFocusable(false);
        this.reg_dob_editText.setClickable(true);
        this.reg_dob_editText.setOnClickListener(this);
        this.reg_countrycode_editText.setLongClickable(false);
        this.reg_countrycode_editText.setFocusable(false);
        this.reg_countrycode_editText.setClickable(true);
        this.reg_countrycode_editText.setOnClickListener(this);
        this.reg_community_button.setOnClickListener(this);
        this.reg_next_button.setOnClickListener(this);
        this.male_button.setOnClickListener(this);
        this.female_button.setOnClickListener(this);
        this.eye.setOnClickListener(this);
        this.mobile_help.setOnClickListener(this);
       // this.reg_login_button.setOnClickListener(this);
        this.reg_email_editText.setOnClickListener(this);
        this.reg_mobile_editText.setOnClickListener(this);
        this.reg_email_editText.setOnTouchListener(this);
        this.reg_mobile_editText.setOnTouchListener(this);


        this.reg_password_editText.setOnFocusChangeListener(new C12491());
        this.reg_name_editText.setOnEditorActionListener(new C12502());


    }

    public static FragmentOne newInstance(){
        FragmentOne fragment = new FragmentOne();
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void createProfile(String key, String value) {

        if(key!=null && value != null) {
            one.setProfilecreate_id(key);
            one.setProfilecreate(value);
        }else {
            LoadingIndicator.getInstance().displayToastMessage(this.context.getResources().getString(R.string.a),this.context);
        }
    }

    private boolean validation(){


        if(one.getName().isEmpty()){
            return false;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        FragmentOne fragmentOne = this;

        switch (v.getId()) {
            case R.id.amrithdhari_button:
                LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());
                break;
            case R.id.female_button:
                storeEditTextValues();
                LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());

                if (one.getDOB().length() > 0) {
                    ModelOne.day = 0;
                    ModelOne.month = 0;
                    ModelOne.year = 0;
                    one.setDOB("");
                    fragmentOne.reg_dob_editText.setText("");
                    }
                fragmentOne.male_button.setBackgroundResource(R.drawable.rounded_border_edittext);
                fragmentOne.male_button.setTextColor(fragmentOne.context.getResources().getColor(R.color.black));
                fragmentOne.female_button.setBackgroundResource(R.drawable.rounded_selected_button);
                fragmentOne.female_button.setTextColor(fragmentOne.context.getResources().getColor(R.color.black));
                one.setGender("2");
                if (fragmentOne.reg_name_editText.getText().length() == 0) {
                    LoadingIndicator.getInstance().showSoftKeyboard(getActivity(), fragmentOne.reg_name_editText);
                    return;
                } else {
                    fragmentOne.reg_dob_editText.performClick();
                    return;
                }
            case R.id.male_button:
                LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());
                storeEditTextValues();
                if (one.getDOB().length() > 0) {
                    ModelOne.day = 0;
                    ModelOne.month = 0;
                    ModelOne.year = 0;
                    one.setDOB("");
                    fragmentOne.reg_dob_editText.setText("");
                }
                fragmentOne.female_button.setBackgroundResource(R.drawable.rounded_border_edittext);
                fragmentOne.female_button.setTextColor(fragmentOne.context.getResources().getColor(R.color.black));
                fragmentOne.male_button.setBackgroundResource(R.drawable.rounded_selected_button);
                fragmentOne.male_button.setTextColor(fragmentOne.context.getResources().getColor(R.color.black));
                one.setGender("1");
                if (fragmentOne.reg_name_editText.getText().length() == 0) {
                    LoadingIndicator.getInstance().showSoftKeyboard(getActivity(), fragmentOne.reg_name_editText);
                    return;
                } else {
                    fragmentOne.reg_dob_editText.performClick();
                    return;
                }

            case R.id.password_view_hide:

                ImageView imageView;
                int i;

                if (fragmentOne.cliked) {
                   // GAAnalyticsOperations.getInstance().sendAnalyticsEvent(fragmentOne.context, fragmentOne.context.getResources().getString(R.string.category_Login), fragmentOne.context.getResources().getString(R.string.action_click), fragmentOne.context.getResources().getString(R.string.label_Password_eye), 1);
                    fragmentOne.reg_password_editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    fragmentOne.cliked = false;
                    imageView = fragmentOne.eye;
                    i = R.drawable.view_password;
                } else {
                  //  GAAnalyticsOperations.getInstance().sendAnalyticsEvent(fragmentOne.context, fragmentOne.context.getResources().getString(R.string.category_Login), fragmentOne.context.getResources().getString(R.string.action_click), fragmentOne.context.getResources().getString(R.string.label_Password_eye), 1);
                    fragmentOne.reg_password_editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    fragmentOne.cliked = true;
                    imageView = fragmentOne.eye;
                    i = R.drawable.view_password_2;
                }
                imageView.setImageResource(i);
                fragmentOne.reg_password_editText.setSelection(fragmentOne.reg_password_editText.getText().length());
                return;
            case R.id.reg_dob_editText:
                LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());
                storeEditTextValues();
               if (one.getGender() == null) {
                    LoadingIndicator.getInstance().displayToastMessage("Please select gender", getActivity());
                    return;
                } else{
                   showDatePickerDialog();
                    return;
                }
            case R.id.reg_email_editText:
                LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());
                if (fragmentOne.reg_email_editText.getText().toString().trim().length() == 0 && one.getProfilecreate_id().equalsIgnoreCase("1") ) {
                    loadHintClicked(1,RC_HINT);
                    return;
                }

                fragmentOne.reg_email_editText.setFocusable(true);
                LoadingIndicator.getInstance().showSoftKeyboard(getActivity(), fragmentOne.reg_email_editText);

            case R.id.reg_next_button:
                LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());
                if (LoadingIndicator.getInstance().isNetAvailable(fragmentOne.context)) {


                }

        }
    }


    private void loadHintClicked(int i,int i2) {

        HintRequest hintRequest = new HintRequest.Builder()
                .setHintPickerConfig(new CredentialPickerConfig.Builder()
                        .setShowCancelButton(true)
                        .build())
                .setIdTokenRequested(true)
                .setEmailAddressIdentifierSupported(true)
                .setAccountTypes(IdentityProviders.GOOGLE)
                .build();

        mCredentialsApiClient = new GoogleApiClient.Builder(getActivity())
               .addConnectionCallbacks(this)
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.CREDENTIALS_API)
                .build();

        PendingIntent intent =
                Auth.CredentialsApi.getHintPickerIntent(mCredentialsApiClient, hintRequest);
        try {
            if(i==1) {
                startIntentSenderForResult(intent.getIntentSender(), i2, null, 0, 0, 0, null);
            }

        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Could not start hint picker Intent", e);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        //hideProgress();

        switch (requestCode) {
            case RC_HINT:
                Credential credential = (Credential) data.getParcelableExtra("com.google.android.gms.credentials.Credential");
                this.reg_name_editText.setText(credential.getName());
                this.reg_email_editText.setText(credential.getId());
                this.reg_password_editText.setText(credential.getPassword());
                return;
        }
    }

    private void storeEditTextValues() {

        one.setName(this.reg_name_editText.getText().toString().trim());
        one.setDOB(this.reg_dob_editText.getText().toString().trim());
        one.setMobile(this.reg_mobile_editText.getText().toString().trim());
        one.setEmail(this.reg_email_editText.getText().toString().trim());
        one.setPassword(this.reg_password_editText.getText().toString().trim());

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended:" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void callSmartSignup() {
        if (one.getProfilecreate_id().equalsIgnoreCase("1")) {
            this.emailNone = true;
            this.mobileNone = true;
            loadHintClicked(1,RC_HINT);
            return;
        }
        this.emailNone = false;
        this.mobileNone = false;
        this.reg_name_editText.setText("");
        this.reg_email_editText.setText("");
        this.reg_mobile_editText.setText("");

        LoadingIndicator.getInstance().showSoftKeyboard(getActivity(), this.reg_name_editText);
    }

    class C12491 implements View.OnFocusChangeListener {
        C12491() {
        }

        public void onFocusChange(View view, boolean z) {
            if (!z) {
                if (reg_password_editText.getText().toString().trim().isEmpty()) {
                    LoadingIndicator.getInstance().displayToastMessage(context.getResources().getString(R.string.password_enter), getActivity());
                } else if (reg_password_editText.getText().toString().trim().length() < 6 || reg_password_editText.getText().toString().trim().length() > 12) {
                    LoadingIndicator.getInstance().displayToastMessage(context.getResources().getString(R.string.password_should_be), getActivity());
                }
            }
        }
    }
    class C12502 implements TextView.OnEditorActionListener {
        C12502() {
        }

        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (i != 5) {
                return false;
            }
            FragmentOne.this.reg_dob_editText.performClick();
            return true;
        }
    }
    class C12524 implements TextView.OnEditorActionListener {
        C12524() {
        }

        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (i != 5) {
                return false;
            }
            FragmentOne.this.reg_dob_editText.performClick();
            return true;
        }
    }
    @SuppressLint({"ValidFragment"})
    private void showDatePickerDialog() {
        int i;
        String str="";
        long j;
        long parseInt;
        StringBuilder stringBuilder;
        this.calendar = Calendar.getInstance();
        Calendar instance = Calendar.getInstance();
        this.demmyCalender = Calendar.getInstance();
        if (one.getGender().equals("1")) {
            if (!ModelOne.regMStartAge.isEmpty()) {
                i = this.calendar.get(Calendar.YEAR);
                str = ModelOne.regMStartAge;
            }
            i = instance.get(Calendar.YEAR);
            j = (long) i-20;
            if (ModelOne.day != 0 || ModelOne.year == 0) {
                this.year = (int) j;
                this.month = this.calendar.get(Calendar.MONTH) + 1;
                i = this.calendar.get(Calendar.DATE);
            } else {
                this.year = ModelOne.year;
                this.month = ModelOne.month;
                i = ModelOne.day;
            }
            this.day = i;
            parseInt = (long) (instance.get(Calendar.YEAR) - Integer.parseInt("70"));
            stringBuilder = new StringBuilder();
            stringBuilder.append(this.year);
            stringBuilder.append("-");
            stringBuilder.append(addZero(this.month));
            stringBuilder.append("-");
            stringBuilder.append(addZero(this.day));
            showCustomDatePicker((int) parseInt, (int) j, stringBuilder.toString());
        }else {
            if (!ModelOne.regFStartAge.isEmpty()) {
                i = this.calendar.get(Calendar.YEAR);
                str = ModelOne.regFStartAge;
            }
            i = instance.get(Calendar.YEAR);
            j = (long) i-18;
            if (ModelOne.day != 0) {
            }
            this.year = (int) j;
            this.month = this.calendar.get(Calendar.MONTH) + 1;
            i = this.calendar.get(Calendar.DATE);
            this.day = i;
            parseInt = (long) (instance.get(Calendar.YEAR) - Integer.parseInt("70"));
            stringBuilder = new StringBuilder();
            stringBuilder.append(this.year);
            stringBuilder.append("-");
            stringBuilder.append(addZero(this.month));
            stringBuilder.append("-");
            stringBuilder.append(addZero(this.day));
            showCustomDatePicker((int) parseInt, (int) j, stringBuilder.toString());
        }
      /*  i -= Integer.parseInt(str);
        j = (long) i;
        if (ModelOne.day != 0) {
        }
        this.year = (int) j;
        this.month = this.calendar.get(Calendar.MONTH) + 1;
        i = this.calendar.get(Calendar.DATE);
        this.day = i;
        parseInt = (long) (instance.get(Calendar.YEAR) - Integer.parseInt("70"));
        stringBuilder = new StringBuilder();
        stringBuilder.append(this.year);
        stringBuilder.append("-");
        stringBuilder.append(addZero(this.month));
        stringBuilder.append("-");
        stringBuilder.append(addZero(this.day));
        showCustomDatePicker((int) parseInt, (int) j, stringBuilder.toString());*/
    }

    private void showCustomDatePicker(int i, int i2, String str) {
       new DatePickerPopWin.Builder(this.context, new C25748()).textConfirm("SET").textCancel("CANCEL").btnTextSize(16).viewTextSize(25).colorCancel(Color.parseColor("#999999")).colorConfirm(Color.parseColor("#009900")).minYear(i).maxYear(i2).dateChose(str).build().showPopWin((Activity) this.context);
    }

    private static String addZero(int i) {
        if (i >= 10) {
            return String.valueOf(i);
        }
        StringBuilder stringBuilder = new StringBuilder("0");
        stringBuilder.append(String.valueOf(i));
        return stringBuilder.toString();
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.calendar = Calendar.getInstance();
        this.currentDate = LoadingIndicator.getInstance().getCurrentDate();
        LoadingIndicator.getInstance().hideSoftKeyboard(getActivity());
    }

    class C25748 implements DatePickerPopWin.OnDatePickedListener{
        C25748() {
        }

        public void onDatePickCompleted(int i, int i2, int i3, String str) {
            ModelOne.year = i;
            ModelOne.month = i2;
            ModelOne.day = i3;
            FragmentOne firstPage_Registration_Fragment = FragmentOne.this;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(ModelOne.year);
            stringBuilder.append("-");
            stringBuilder.append(FragmentOne.addZero(ModelOne.month));
            stringBuilder.append("-");
            stringBuilder.append(FragmentOne.addZero(ModelOne.day));
            firstPage_Registration_Fragment.DOBdate = stringBuilder.toString();
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append(ModelOne.year);
            stringBuilder2.append("-");
            stringBuilder2.append(FragmentOne.addZero(ModelOne.month));
            stringBuilder2.append("-");
            stringBuilder2.append(FragmentOne.addZero(ModelOne.day));
            ModelOne.dateOfBirth = stringBuilder2.toString();
            try {
                ModelOne.differentialYears = LoadingIndicator.getInstance().compareTwoDates(FragmentOne.this.dateFormat.parse(FragmentOne.this.currentDate), FragmentOne.this.dateFormat.parse(ModelOne.dateOfBirth), FragmentOne.this.demmyCalender);
            } catch (Exception e) {
                e.printStackTrace();
            }
            EditText access$500 = FragmentOne.this.reg_dob_editText;
            stringBuilder = new StringBuilder();
            stringBuilder.append(ModelOne.differentialYears);
            stringBuilder.append(" yrs/ ");
            stringBuilder.append(LoadingIndicator.getInstance().convertDateTimeFormat(ModelOne.dateOfBirth));
            access$500.setText(stringBuilder.toString());
            if (FragmentOne.this.reg_email_editText.getText().toString().length() == 0) {
                access$500 = FragmentOne.this.reg_email_editText;
            } else {
                if (FragmentOne.this.reg_password_editText.getText().toString().length() == 0) {
                    access$500 = FragmentOne.this.reg_password_editText;
                }
                ((InputMethodManager) FragmentOne.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(1, 0);
            }
            access$500.requestFocus();
            ((InputMethodManager) FragmentOne.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(1, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mCredentialsApiClient.stopAutoManage(getActivity());
        mCredentialsApiClient.disconnect();
    }
}
