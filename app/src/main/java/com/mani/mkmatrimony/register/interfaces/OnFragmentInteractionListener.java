package com.mani.mkmatrimony.register.interfaces;

import android.support.v4.app.Fragment;

public interface OnFragmentInteractionListener {
    public void onFragmentInteraction(int size, Fragment f);
}
