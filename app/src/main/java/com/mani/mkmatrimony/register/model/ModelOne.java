package com.mani.mkmatrimony.register.model;



public class ModelOne {


    public static String dateOfBirth;
    public static int differentialYears;
    private String Name;
    private String DOB;
    private String Age;
    private String gender;
    private String email;
    private String password;
    private String mobile;
    private String religion;
    private String caste;
    private String subcaste;
    public static int day = 0;
    public static int month = 0;
    public static int year;
    public static String regMStartAge = "";
    public static String regFStartAge = "";


    public String getProfilecreate() {
        return profilecreate;
    }

    public void setProfilecreate(String profilecreate) {
        this.profilecreate = profilecreate;
    }

    public String getProfilecreate_id() {
        return profilecreate_id;
    }

    public void setProfilecreate_id(String profilecreate_id) {
        this.profilecreate_id = profilecreate_id;
    }

    private String profilecreate;
    private String profilecreate_id;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getSubcaste() {
        return subcaste;
    }

    public void setSubcaste(String subcaste) {
        this.subcaste = subcaste;
    }
}
