package com.mani.mkmatrimony.register.adapter;

import android.content.Context;
import android.graphics.Color;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.mani.mkmatrimony.R;
import com.mani.mkmatrimony.register.fragment.FragmentOne;

import java.util.HashMap;

public class RegisterButtonAdapter extends BaseAdapter {

    String[] dataArrays = null;
    private LayoutInflater inflator;
    private Context mcontext;
    private itemClick i;
    public HashMap<Integer, Boolean> hashMapSelected;

    private SmartSignInterface anInterface;
    class C11961 implements View.OnClickListener {
        private StringBuilder stringBuilder;

        C11961() {
        }

        @Override
        public void onClick(View v) {

                stringBuilder = new StringBuilder();
                stringBuilder.append(v.getTag());
                 makeAllUnselect(v.getId());
                notifyDataSetChanged();
                i.createProfile(stringBuilder.toString(),RegisterButtonAdapter.this.dataArrays[v.getId()].split(",")[1]);
                anInterface.callSmartSignup();


        }
    }

    public RegisterButtonAdapter(String[] strArr, Context context,SmartSignInterface anInterface) {

        this.dataArrays = strArr;
        this.mcontext = context;
        this.inflator = LayoutInflater.from(context);
        this.anInterface=anInterface;
        hashMapSelected = new HashMap<>();
        for (int i = 0; i < strArr.length; i++) {
            hashMapSelected.put(i, false);
        }

    }

    public void makeAllUnselect(int position) {
        hashMapSelected.put(position, true);
        for (int i = 0; i < hashMapSelected.size(); i++) {
            if (i != position)
                hashMapSelected.put(i, false);
        }
    }

    @Override
    public int getCount() {
        return this.dataArrays.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        view = this.inflator.inflate(R.layout.lay_register_adapter_button, null);
        Button button = (Button) view.findViewById(R.id.Common_btn);
        button.setId(i);
        button.setBackgroundColor(Color.BLUE);
        button.setTag(this.dataArrays[i].split(",")[0]);

        if (hashMapSelected.get(i) == true) {
            button.setBackgroundColor(Color.BLUE);
        } else {
            button.setBackgroundResource(R.drawable.rounded_border_edittext);
        }

        button.setText(this.dataArrays[i].split(",")[1]);
        button.setOnClickListener(new C11961());
        return view;
    }
     public void  getValuse(itemClick i){
        this.i=i;
    }

    public interface SmartSignInterface {
        void callSmartSignup();
    }

    public interface itemClick{
            public void createProfile(String key,String value);
    }

}
