package com.mani.mkmatrimony.register.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mani.mkmatrimony.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class FragmentFour extends Fragment implements Step {
    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.register_four, container, false);

        return view;
    }

    public static FragmentFour newInstance() {
        FragmentFour four =new FragmentFour();
        return four;
    }
}
