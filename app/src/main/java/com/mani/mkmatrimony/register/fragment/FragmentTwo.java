package com.mani.mkmatrimony.register.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mani.mkmatrimony.R;
import com.mani.mkmatrimony.register.Otp_VerificationActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

public class FragmentTwo extends Fragment implements Step {
    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        Toast.makeText(getActivity(),"eee",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.registertwo, container, false);

        //String title = this.getArguments().getString("FragmentOne");
        //Toast.makeText(getActivity(),""+title,Toast.LENGTH_LONG).show();
        return view;
    }

    public static FragmentTwo newInstance() {

        FragmentTwo two  = new FragmentTwo();
        return two;
    }
}
