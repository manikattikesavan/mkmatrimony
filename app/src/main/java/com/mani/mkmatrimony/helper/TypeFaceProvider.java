package com.mani.mkmatrimony.helper;

import android.content.Context;
import android.graphics.Typeface;
import java.util.Hashtable;

public class TypeFaceProvider {
    private static final Hashtable<String, Typeface> cache = new Hashtable();

    public static Typeface getFontForTextView(Context context, String str) {
        Typeface typeface;
        synchronized (cache) {
            if (!cache.containsKey(str)) {
                cache.put(str, Typeface.createFromAsset(context.getAssets(), str));
            }
            typeface = (Typeface) cache.get(str);
        }
        return typeface;
    }
}
