package com.mani.mkmatrimony.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class CustomTextView extends AppCompatTextView {
    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";
    private Typeface customFont;
    private int textStyle;

    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        applyCustomFont(context, attributeSet);
    }

    public CustomTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        applyCustomFont(context, attributeSet);
    }

    private void applyCustomFont(Context context, AttributeSet attributeSet) {
        this.textStyle = attributeSet.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", 0);
        this.customFont = selectTypeface(context, this.textStyle);
        setTypeface(this.customFont);
    }

    private Typeface selectTypeface(Context context, int i) {
        return TypeFaceProvider.getFontForTextView(context, i != 1 ? "fonts/Montserrat-Light.otf" : "fonts/Montserrat-Regular.otf");
    }
}
