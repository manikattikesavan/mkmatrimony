package com.mani.mkmatrimony.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class FadeScrollView extends ScrollView {
    private boolean enableScrolling = true;

    public FadeScrollView(Context context) {
        super(context);
    }

    public FadeScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public FadeScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    protected float getBottomFadingEdgeStrength() {
        return 0.0f;
    }

    protected float getTopFadingEdgeStrength() {
        return 0.0f;
    }

    public boolean isEnableScrolling() {
        return this.enableScrolling;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return isEnableScrolling() ? super.onInterceptTouchEvent(motionEvent) : false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnableScrolling() ? super.onTouchEvent(motionEvent) : false;
    }

    public boolean performClick() {
        super.performClick();
        return true;
    }

    public void setEnableScrolling(boolean z) {
        this.enableScrolling = z;
    }
}
