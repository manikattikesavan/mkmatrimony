package com.mani.mkmatrimony.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

public class CustomButton extends AppCompatButton {
    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setFont();
    }

    private void setFont() {
        setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Regular.otf"), 0);
    }
}
