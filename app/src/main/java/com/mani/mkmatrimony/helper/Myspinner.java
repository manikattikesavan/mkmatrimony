package com.mani.mkmatrimony.helper;

import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.widget.AdapterView.OnItemSelectedListener;

public class Myspinner extends AppCompatSpinner {
    OnItemSelectedListener listener;

    public Myspinner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.listener = onItemSelectedListener;
    }

    public void setSelection(int i) {
        super.setSelection(i);
        if (i == getSelectedItemPosition()) {
            this.listener.onItemSelected(null, null, i, 0);
        }
    }
}
