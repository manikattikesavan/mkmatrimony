package com.mani.mkmatrimony.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.mani.mkmatrimony.MyApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class LoadingIndicator {


    private static ProgressDialog mProgressDialog;

    private static LoadingIndicator  li;

    public static LoadingIndicator getInstance() {
        if (li == null) {
            li = new LoadingIndicator();
        }
        return li;
    }

    public void showLoading(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(message);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    public void dismissLoading() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void displayToastMessage(String str, Context context) {
        if (context != null) {
            Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNetAvailable(Context context) {
        if (context == null) {
            context = MyApplication.getInstance().getContext();
        }
        return checkIsNetAvailable(context) && checkTheNetworkAvailability(context);
    }

    public boolean checkIsNetAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager == null ? false : connectivityManager.getNetworkInfo(1).isAvailable() || connectivityManager.getNetworkInfo(0).isAvailable();
    }

    public boolean checkTheNetworkAvailability(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        if (allNetworkInfo != null) {
            for (NetworkInfo state : allNetworkInfo) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        } catch (Exception e) {
            e.getMessage();
        }
    }
    public void showSoftKeyboard(Activity activity, EditText editText) {
        try {
            editText.requestFocus();
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(editText, 0);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public int compareTwoDates(Date date, Date date2, Calendar calendar) {
        Calendar calendar2 = getCalendar(date);
        Calendar calendar3 = getCalendar(date2);
        int i = calendar2.get(Calendar.YEAR) - calendar3.get(Calendar.YEAR);
        if (calendar2.get(Calendar.MONTH) >= calendar3.get(Calendar.MONTH)) {
            if (calendar2.get(Calendar.MONTH) > calendar3.get(Calendar.MONTH) || calendar2.get(Calendar.DATE) >= calendar3.get(Calendar.DATE)) {
                if (calendar2.get(Calendar.MONTH) == calendar3.get(Calendar.MONTH) && calendar3.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && calendar2.get(Calendar.DATE) < calendar3.get(Calendar.DATE)) {
                    i--;
                }
                return i;
            }
        }
        return i - 1;
    }
    public Calendar getCalendar(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        return instance;
    }
    public String getCurrentDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public String convertDateTimeFormat(String str) {
        try {
            return new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyy-MM-dd").parse(str));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


}